<?php

namespace BitNinja\NinjaReCaptcha;

class GReCaptchaFactoryTest extends \PHPUnit_Framework_TestCase
{
    
    private $logger;
    
    protected function setUp()
    {
        parent::setUp();
        $this->logger = new \Devedge\Log\NoLog;
    }
    
    public function testCreateChallangeViewCreatesChallangeView()
    {
        //GIVEN
        $underTest = new GReCaptchaFactory("banana", "banana", $this->logger);
        //WHEN
        $challengeView = $underTest->createChallengeView();
        //WILL
        $this->assertTrue(
            $challengeView instanceof GReCaptchaChallengeView,
            "The entity returned by createChallengeView is not an instance of GReCaptchaChallengeview."
        );
    }
    
    public function testCreateChallangeVerifyerCreatesChallangeVerifyer()
    {
        //GIVEN
        $underTest = new GReCaptchaFactory("banana", "banana", $this->logger);
        //WHEN
        $challengeVerifyer = $underTest->createChallengeVerifier();
        //WILL
        $this->assertTrue(
            $challengeVerifyer instanceof GReCaptchaChallengeVerifier,
            "The entity returned by createChallengeView is not an instance of GReCaptchaChallengeVerifier."
        );
    }
}
