<?php

namespace BitNinja\NinjaReCaptcha;

class GReCaptchaChallengeViewTest extends \PHPUnit_Framework_TestCase
{

    private $logger;
    
    protected function setUp()
    {
        parent::setUp();
        $this->logger = new \Devedge\Log\NoLog;
    }
    
    public function testRenderResultWillContainGivenAction()
    {
        //GIVEN
        $challengeView = new GReCaptchaChallengeView("siteKey", $this->logger);
        $verificationUrl = 'http://verification.hu/YouAreTheBananaKing.php';
        //WHEN
        $renderResult = $challengeView->render($verificationUrl);
        //WILL
        $this->assertEquals(1, preg_match("@action=['\"]".$verificationUrl."['\"]@", $renderResult));
    }
    
}
