<?php

namespace BitNinja\NinjaReCaptcha;

class GRecaptchaChallengeVerifierTest extends \PHPUnit_Framework_TestCase
{
    private $logger;
    
    protected function setUp()
    {
        parent::setUp();
        $this->logger = new \Devedge\Log\NoLog;
    }
    
    public function testVerifyWillCallReCaptchaVerification()
    {
        //GIVEN
        $reCaptchaMock = $this->getMockBuilder(\ReCaptcha\ReCaptcha::class)
            ->disableOriginalConstructor()
            ->getMock();
        $reCaptchaMock->method("verify")->willReturn(new \ReCaptcha\Response(true));
        $challengeVerifier = new GReCaptchaChallengeVerifier($reCaptchaMock, $this->logger);
        //WILL
        $reCaptchaMock->expects($this->atLeastOnce())->method("verify");
        //WHEN
        $response = array("g-recaptcha-response"=>"This test shall pass.");
        $challengeVerifier->verify($response, "1.2.3.4");
    }
    
    public function testVerifyWillReturnFalseWhenReCaptchaVerificationFails()
    {
        //GIVEN
        $reCaptchaMock = $this->getMockBuilder(\ReCaptcha\ReCaptcha::class)
            ->disableOriginalConstructor()
            ->getMock();
        $reCaptchaMock->method("verify")->willReturn(new \ReCaptcha\Response(false));
        $challengeVerifier = new GReCaptchaChallengeVerifier($reCaptchaMock, $this->logger);
        //WHEN
        $response = array("g-recaptcha-response"=>"This test shall pass.");
        $result = $challengeVerifier->verify($response, "1.2.3.4");
        //WILL
        $this->assertFalse($result);
    }
    
	public function testVerifyWillReturnFalseWhenGReCaptchaResponseIsMissingFails()
    {
        //GIVEN
        $reCaptchaMock = $this->getMockBuilder(\ReCaptcha\ReCaptcha::class)
            ->disableOriginalConstructor()
            ->getMock();
        $reCaptchaMock->method("verify")->willReturn(new \ReCaptcha\Response(false));
        $challengeVerifier = new GReCaptchaChallengeVerifier($reCaptchaMock, $this->logger);
        //WHEN
        $response = array("not-valid"=>"This test shall fail.");
        $result = $challengeVerifier->verify($response, "1.2.3.4");
        //WILL
        $this->assertFalse($result);
    }
	
    public function testVerifyWillReturnTrueWhenReCaptchaVerificationSucceeds()
    {
        //GIVEN
        $reCaptchaMock = $this->getMockBuilder(\ReCaptcha\ReCaptcha::class)
            ->disableOriginalConstructor()
            ->getMock();
        $reCaptchaMock->method("verify")->willReturn(new \ReCaptcha\Response(true));
        $challengeVerifier = new GReCaptchaChallengeVerifier($reCaptchaMock, $this->logger);
        //WHEN
        $response = array("g-recaptcha-response"=>"This test shall pass.");
        $result = $challengeVerifier->verify($response, "1.2.3.4");
        //WILL
        $this->assertTrue($result);
    }
}
