<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BitNinja\NinjaReCaptcha\HtmlNodes;

/**
 * Description of CDataHtmlNode
 *
 * @author operator
 */
class CDataHtmlNode extends AbstractHtmlNode
{

    protected $text;

    public function __construct($text)
    {
        $this->text = $text;
    }

    public function render()
    {
        return $this->text;
    }
}
