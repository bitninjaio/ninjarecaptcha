<?php

namespace BitNinja\NinjaReCaptcha\HtmlNodes;

class EscapedAttributeSet implements AttributeSet
{
    protected $attributes = array();

    public function setAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
        return $this;
    }

    public function renderAttributes()
    {
        $keyValuePairs = array();
        foreach ($this->attributes as $key => $value) {
            $keyValuePairs[] = htmlspecialchars($key) . '="' . htmlspecialchars($value) . '"';
        }
        return implode(" ", $keyValuePairs);
    }
}
