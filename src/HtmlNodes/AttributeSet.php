<?php


namespace BitNinja\NinjaReCaptcha\HtmlNodes;

interface AttributeSet {
    public function setAttribute($key, $value);
    public function renderAttributes();
}
