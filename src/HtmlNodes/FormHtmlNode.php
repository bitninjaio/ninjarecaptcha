<?php

namespace BitNinja\NinjaReCaptcha\HtmlNodes;

class FormHtmlNode extends AbstractHtmlNode{

    public function __construct($action, $method, array $attributes) {
        $this->getAttrs()->setAttribute("action", $action);
        $this->getAttrs()->setAttribute("method", $method);
        foreach ($attributes as $attrName=>$attrValue)
        {
            $this->getAttrs()->setAttribute($attrName, $attrValue);
        }
    }

    public function render() {
        return "<form ".$this->getAttrs()->renderAttributes()." >".$this->renderInnerNodes()."</form>";
    }

}
