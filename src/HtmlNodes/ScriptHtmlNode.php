<?php

namespace BitNinja\NinjaReCaptcha\HtmlNodes;

class ScriptHtmlNode extends AbstractHtmlNode {
    
    public function render() {
        return "<script ".$this->getAttrs()->renderAttributes()." >".$this->renderInnerNodes()."</script>";
    }

}
