<?php

namespace BitNinja\NinjaReCaptcha\HtmlNodes;

abstract class AbstractHtmlNode implements HtmlNode
{

    protected $innerNodes = array();
    protected $attrs = NULL;

    public function addInnerNode(HtmlNode $node)
    {
        $this->innerNodes[] = $node;
        return $this;
    }

    public function setAttributes(AttributeSet $attrs)
    {
        $this->attrs = $attrs;
        return $this;
    }

    public function getAttrs()
    {
        if ($this->attrs === NULL) {
            $this->attrs = new EscapedAttributeSet();
        }
        return $this->attrs;
    }

    protected function renderInnerNodes()
    {
        $result = "";
        foreach ($this->innerNodes as $node) {
            $result .= $node->render();
        }
        return $result;
    }
}
