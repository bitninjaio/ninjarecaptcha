<?php

namespace BitNinja\NinjaReCaptcha\HtmlNodes;


class DivHtmlNode extends AbstractHtmlNode {
    public function render() {
        return "<div ".$this->getAttrs()->renderAttributes()." >".$this->renderInnerNodes()."</div>";
    }
}
