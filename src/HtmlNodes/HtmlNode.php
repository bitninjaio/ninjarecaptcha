<?php

namespace BitNinja\NinjaReCaptcha\HtmlNodes;

interface HtmlNode {
    
    public function setAttributes(AttributeSet $attrs);
    public function addInnerNode(HtmlNode $node);
    public function render();
    
}
