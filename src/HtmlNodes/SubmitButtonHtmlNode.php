<?php

namespace BitNinja\NinjaReCaptcha\HtmlNodes;

class SubmitButtonHtmlNode extends AbstractHtmlNode {
    
    public function __construct($name, $value) {
        $this->getAttrs()->setAttribute("type", "submit");
        $this->getAttrs()->setAttribute("name", $name);
        $this->getAttrs()->setAttribute("value", $value);
    }
    
    public function render() {
        return "<input ".$this->attrs->renderAttributes()." >".$this->renderInnerNodes()."</input>";
    }

}
