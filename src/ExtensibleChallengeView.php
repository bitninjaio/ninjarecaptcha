<?php

namespace BitNinja\NinjaReCaptcha;


class ExtensibleChallengeView implements CaptchaChallengeView
{

    const ITEM_POSITION_START = 1;
    const ITEM_POSITION_END = 2;

    protected $htmlNodesBeforeForm = array();
    protected $htmlNodesInForm = array();
    protected $htmlNodesAfterForm = array();
    protected $formAttrs = array();
    protected $logger;

    public function __construct(\Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function render($verificationFormUri)
    {
        $this->logger->debug("Rendering challenge page...");
        $output = "";

        foreach ($this->htmlNodesBeforeForm as $node) {
            $output .= $node->render();
        }

        $formNode = new HtmlNodes\FormHtmlNode($verificationFormUri, "POST", $this->formAttrs);
        foreach ($this->htmlNodesInForm as $node) {
            $formNode->addInnerNode($node);
        }
        $output .= $formNode->render();

        foreach ($this->htmlNodesAfterForm as $node) {
            $output .= $node->render();
        }
        $this->logger->debug("Rendering finished.");
        return $output;
    }

    protected function placeInNodeArray($item, &$array, $itemPosition)
    {
        switch ($itemPosition) {
            case self::ITEM_POSITION_START:
                array_unshift($array, $item);
                break;
            case self::ITEM_POSITION_END:
                array_push($array, $item);
                break;
            default:
                break;
        }
    }

    public function addNodeBeforeForm(HtmlNodes\HtmlNode $node, $itemPosition = self::ITEM_POSITION_END)
    {
        $this->placeInNodeArray($node, $this->htmlNodesBeforeForm, $itemPosition);
        return $this;
    }

    public function setFormAttrs(array $attributes)
    {
        $this->formAttrs = $attributes;
        return $this;
    }

    public function addNodeInForm(HtmlNodes\HtmlNode $node, $itemPosition = self::ITEM_POSITION_END)
    {
        $this->placeInNodeArray($node, $this->htmlNodesInForm, $itemPosition);
        return $this;
    }

    public function addNodeAfterForm(HtmlNodes\HtmlNode $node, $itemPosition = self::ITEM_POSITION_END)
    {
        $this->placeInNodeArray($node, $this->htmlNodesAfterForm, $itemPosition);
        return $this;
    }
}
