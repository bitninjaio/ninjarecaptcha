<?php

namespace BitNinja\NinjaReCaptcha;

class ExtensibleGReCaptchaChallengeView extends ExtensibleChallengeView
{
    protected $logger;
    protected $siteKey;

    public function __construct($siteKey, \Psr\Log\LoggerInterface $logger)
    {
        parent::__construct($logger);
        $this->logger = $logger;
        $this->siteKey = $siteKey;
    }

    public function render($verificationFormUri)
    {
        $this->logger->debug("Rendered view for site with key [$this->siteKey]");
        $scriptNode = new HtmlNodes\ScriptHtmlNode();
        $scriptNode->getAttrs()
            ->setAttribute("src", "https://recaptcha.net/recaptcha/api.js")
            ->setAttribute("async", "true")
            ->setAttribute("defer", "true");

        $this->addNodeBeforeForm($scriptNode);

        $gRecaptchaDivNode = new HtmlNodes\DivHtmlNode();
        $gRecaptchaDivNode->getAttrs()
            ->setAttribute("class", "g-recaptcha")
            ->setAttribute("data-sitekey", $this->siteKey);

        $this->addNodeInForm($gRecaptchaDivNode, ExtensibleChallengeView::ITEM_POSITION_START);

        return parent::render($verificationFormUri);
    }
}
