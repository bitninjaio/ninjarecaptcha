<?php

namespace BitNinja\NinjaReCaptcha;

/**
 * Classes implementing this interface should be used to render the
 * widget onto the webpage.
 */

interface CaptchaChallengeView
{
    /**
     * Returns the HTML code for the captcha widget.
     *
     * @return String The HTML code.
     */
    public function render($verificationFormUri);
}
