<?php
namespace BitNinja\NinjaReCaptcha;

class GReCaptchaChallengeVerifier implements CaptchaChallengeVerifier
{

    protected $logger;
    protected $reCaptcha;

    public function __construct(\ReCaptcha\ReCaptcha $reCaptcha, \Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->reCaptcha = $reCaptcha;
    }

    public function verify($response, $remoteIp)
    {
        $this->logger->info("Verifying captcha response from " . $remoteIp);

        if (! array_key_exists("g-recaptcha-response", $response)){
            $this->logger->info(
                "Unsuccessful captcha from [" . $remoteIp . "]. Missing property" .
                " [g-recaptcha-response]. Response was: [". substr(var_export($response, true), 0, 99) ."]"
            );

            return false;
        }

        try {
            $verificationResult = $this->reCaptcha->verify($response["g-recaptcha-response"], $remoteIp);

            if ($verificationResult->isSuccess()) {
                return true;
            }
        } catch (\Exception $e) {
            $this->logger->error('Captcha verification error occured:' . $e->getMessage());
            return false;
        }

        $this->logger->info("Unsuccessful captcha from " . $remoteIp . ": [".implode(", ", $verificationResult->getErrorCodes()) . "]");

        return false;
    }
}
