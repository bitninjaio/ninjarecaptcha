<?php

namespace BitNinja\NinjaReCaptcha;

/**
 * Classes implementing this interface should
 * be used to verify the response of the user.
 */

interface CaptchaChallengeVerifier
{
    /**
     * Decides whether the captcha was filled successfully using the information
     * from the browser.
     *
     * @param type $response The response of the user in the same form as it is in the POST variable.
     * @param type $remoteIp The IP of the user
     * @return bool True if the captcha was filled successfully, false otherwise
     */
    public function verify($response, $remoteIp);
}
