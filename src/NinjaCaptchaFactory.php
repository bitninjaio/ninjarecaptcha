<?php

namespace BitNinja\NinjaReCaptcha;

/**
 * To ensure that the ChallangeView is compatible with the
 * ChallangeVerifyer, none of them should be instantiated directly.
 * Instead, you should use an implementation of this interface.
 */

interface NinjaCaptchaFactory
{
    /**
     * This method should create and return an instance of a ChallengeView
     * descendant, from which the data can be directly used as a parameter for
     * the ChallengeVerifyer returned by the createChallengeVerifyer method.
     * @return ChallengeView The created instance.
     */
    public function createChallengeView();

    /**
     * This method should create and return an instance of a ChallengeVerifyer
     * descendant. It should be able to handle the data sent by the ChallangeView
     * returned by the createChallengeView.
     */
    public function createChallengeVerifier();

    /**
     * Set the request method, if needed. Related issues:
     * @link https://github.com/google/recaptcha/issues/359
     * @link https://github.com/google/recaptcha/issues/103
     *
     * @param \ReCaptcha\RequestMethod $method
     * @return static
     */
    public function setRequestMethod($requestMethod);
}
