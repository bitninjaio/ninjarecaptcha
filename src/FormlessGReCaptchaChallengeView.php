<?php

namespace BitNinja\NinjaReCaptcha;


class FormlessGReCaptchaChallengeView implements CaptchaChallengeView
{

    protected $logger;
    protected $siteKey;

    public function __construct($siteKey, \Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->siteKey = $siteKey;
    }


    public function render($verificationFormUri)
    {
        $this->logger->debug("Rendered view for site with key [$this->siteKey]");
        $output = '<script src="https://recaptcha.net/recaptcha/api.js" async defer></script>
                    <div class="g-recaptcha" data-sitekey="' . $this->siteKey . '"></div>';
        return $output;
    }
}
