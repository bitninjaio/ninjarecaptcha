<?php

namespace BitNinja\NinjaReCaptcha;

/**
 *
 */
class GReCaptchaFactory implements NinjaCaptchaFactory
{

    protected $secret;
    protected $sitekey;
    protected $logger;
    protected $requestMethod;

    /**
     * @inheritDoc
     */
    public function setRequestMethod($requestMethod)
    {
        $this->requestMethod = $requestMethod;
        return $this;
    }

    /**
     *
     * @param type                                             $secret
     * @param type                                             $sitekey
     * @param \BitNinja\NinjaReCaptcha\Psr\Log\LoggerInterface $logger
     */
    public function __construct($secret, $sitekey, \Psr\Log\LoggerInterface $logger)
    {
        $this->secret = $secret;
        $this->sitekey = $sitekey;
        $this->logger = $logger;
    }

    /**
     *
     * @return \BitNinja\NinjaReCaptcha\GReCaptchaChallengeVerifier
     */
    public function createChallengeVerifier()
    {
        $this->logger->debug("Creating instance of GReCaptchaChallengeVerifier.");
        $reCaptcha = new \ReCaptcha\ReCaptcha($this->secret, $this->requestMethod);
        return new GReCaptchaChallengeVerifier($reCaptcha, $this->logger);
    }

    public function createChallengeView()
    {
        $this->logger->debug("Creating instance of ChallengeView.");
        return new GReCaptchaChallengeView($this->sitekey, $this->logger);
    }
}
